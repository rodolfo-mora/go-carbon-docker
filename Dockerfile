# https://github.com/go-graphite/go-carbon/blob/master/Dockerfile

FROM golang:1.15.3-alpine3.12 AS build

ARG gocarbon_repo=https://github.com/go-graphite/go-carbon.git

# Git tags
# https://gist.github.com/rponte/fdc0724dd984088606b0

RUN apk add --update git make \
 && git clone "${gocarbon_repo}" /usr/local/src/go-carbon \
 && cd /usr/local/src/go-carbon \
 && export GOCARBON_VERSION=$(git describe --abbrev=0 --tags) \
 && echo "Building $GOCARBON_VERSION" \
 && git checkout tags/"${GOCARBON_VERSION}" \
 && make \
 && chmod +x go-carbon && cp -fv go-carbon /tmp

FROM alpine:3.9
COPY --from=build /tmp/go-carbon /usr/sbin/go-carbon

RUN apk update
RUN apk add --no-cache --update python3
RUN mkdir /var/log/go-carbon /var/lib/graphite /var/lib/graphite/whisper \
    /var/lib/graphite/tagging /var/lib/graphite/dump /etc/go-carbon/

RUN touch /var/run/go-carbon.pid

RUN addgroup -S carbon && adduser -S carbon -G carbon
RUN chown -R carbon:carbon /etc/go-carbon/ /var/lib/graphite/ /var/log/go-carbon \
    /var/run/ /usr/sbin/go-carbon

USER carbon
EXPOSE 2003 2004 7002 7003 7007 8081 2003/udp
CMD ["/usr/sbin/go-carbon", "-daemon=false", "-config", "/etc/go-carbon/go-carbon.conf"]
# ENTRYPOINT ["/bin/sh", "/etc/go-carbon/entrypoint.sh"]
